{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{literal}
<script type="text/javascript">
function customAcceptCookie(){
	document.getElementById("footer_cookie_bar_block").style.display = 'none';
	createCookie('cookiebar','yes',100);
}
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}
</script>
{/literal}


{if !isset($content_only) || !$content_only}
					
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- #center_column -->
					{if isset($HOOK_FOOTER)}
						<!-- Footer -->
						<div class="footer-container col-xs-12 col-sm-10 pull-right clearfix">
							<footer id="footer" class="clearfix">
								{$HOOK_FOOTER}
							</footer>
						</div><!-- #footer -->
					{/if}
			</div><!-- .columns-container -->
		</div><!-- #page -->
{/if}

<div id="footer_cookie_bar_block" style="display: none;">
	<div class="cookiespopup">
		<div class="cookiespopup-content">
			<br>
			En utilisant notre site, vous acceptez notre <a href="http://www.deco-prive.com/content/63-cookie" class="ilightbox_popin">politique de cookies</a>.
		</div>
		<a href="javascript:customAcceptCookie()" class="cookiespopupclosebtn" >X</a>
	</div>
</div>

{include file="$tpl_dir./global.tpl"}
	</body>
</html>