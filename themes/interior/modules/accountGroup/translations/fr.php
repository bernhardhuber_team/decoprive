<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{accountgroup}interior>accountgroup_eb1424cddd82bc4c65568a43d5e5c9ee'] = 'Groupe dans my-account';
$_MODULE['<{accountgroup}interior>accountgroup_4774ecf3aacc118bb13a869666ae14e7'] = 'Ajoute le groupe du client ainsi que la réduction associée dans le module my-account';
$_MODULE['<{accountgroup}interior>accountgroup_fa214007826415a21a8456e3e09f999d'] = 'Etes vous sur de vouloir supprimer vos preferences ?';
$_MODULE['<{accountgroup}interior>accountgroup_4f46990137d81fd87069880c166ee463'] = 'Votre compte est associé au groupe';
$_MODULE['<{accountgroup}interior>accountgroup_7251bc9140cafb444f563603a72e8ba7'] = 'et bénéficie d\'une réduction de';
