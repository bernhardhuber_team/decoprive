{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($categoryProducts) > 0 && $categoryProducts !== false}
<section class="page-product-box blockproductscategory">
	<h3 class="productscategory_h3 page-product-heading">{l s='Other products' mod='productscategory'}</h3>
	<div id="productscategory_list" class="scroll_container clearfix">
		<ul id="bxslider1" class="scroll_carousel product_list grid clearfix">
		 {foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
			<li class="ajax_block_product col-xs-25 col-lg-3 col-sm-4 col-xs-6">
			<div class="product-container" itemscope itemtype="http://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link"	href="{$categoryProduct.link|escape:'html':'UTF-8'}" title="{$categoryProduct.name|escape:'html':'UTF-8'}" itemprop="url">
							<img class="replace-2x img-responsive" src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($categoryProduct.legend)}{$categoryProduct.legend|escape:'html':'UTF-8'}{else}{$categoryProduct.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($categoryProduct.legend)}{$categoryProduct.legend|escape:'html':'UTF-8'}{else}{$categoryProduct.name|escape:'html':'UTF-8'}{/if}" {if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
						</a>
						{if isset($quick_view) && $quick_view}
						<a class="quick-view hidden-xs" href="{$categoryProduct.link|escape:'html':'UTF-8'}" rel="{$categoryProduct.link|escape:'html':'UTF-8'}">
							<span class="font-eye"></span>
						</a>
						{/if}
						{if ($categoryProduct.allow_oosp || $categoryProduct.quantity > 0)}
						{if isset($categoryProduct.new) && $categoryProduct.new == 1}
							<a class="tag new" href="{$categoryProduct.link|escape:'html':'UTF-8'}">
								{l s='New' mod='productscategory'}
							</a>
						{/if}
						{if isset($categoryProduct.on_sale) && $categoryProduct.on_sale && isset($categoryProduct.show_price) && $categoryProduct.show_price && !$PS_CATALOG_MODE}
							<a class="tag sale" href="{$categoryProduct.link|escape:'html':'UTF-8'}">
								{l s='Sale!' mod='productscategory'}
							</a>
						{/if}
						{/if}
						{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($categoryProduct.show_price) && $categoryProduct.show_price) || (isset($categoryProduct.available_for_order) && $categoryProduct.available_for_order)))}
						{if isset($categoryProduct.available_for_order) && $categoryProduct.available_for_order && !isset($restricted_country_mode)}
							<span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="availability">
								{if ($categoryProduct.allow_oosp || $categoryProduct.quantity > 0)}
									{*<span class="{if $categoryProduct.quantity <= 0 && !$categoryProduct.allow_oosp}out-of-stock{else}available-now{/if}">
										<link itemprop="availability" href="http://schema.org/InStock" />{if $categoryProduct.quantity <= 0}{if $categoryProduct.allow_oosp}{if isset($categoryProduct.available_later) && $categoryProduct.available_later}{$categoryProduct.available_later}{else}{l s='In Stock' mod='productscategory'}{/if}{else}{l s='Out of stock' mod='productscategory'}{/if}{else}{if isset($categoryProduct.available_now) && $categoryProduct.available_now}{$categoryProduct.available_now}{else}{l s='In Stock' mod='productscategory'}{/if}{/if}
									</span>*}
								{elseif (isset($categoryProduct.quantity_all_versions) && $categoryProduct.quantity_all_versions > 0)}
									<span class="tag out-of-stock">
										<link itemprop="availability" href="http://schema.org/LimitedAvailability" />{l s='Out of' mod='productscategory'}
										<span>{l s='stock' mod='productscategory'}</span>
									</span>
								{else}
									<span class="tag out-of-stock">
										<link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of' mod='productscategory'}
										<span>{l s='stock' mod='productscategory'}</span>
									</span>
								{/if}
							</span>
						{/if}
					{/if}
					{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices && isset($categoryProduct.specific_prices.reduction) && $categoryProduct.specific_prices.reduction > 0}
						{if $categoryProduct.specific_prices.reduction_type == 'percentage'}
								<span class="tag price-percent-reduction">{$categoryProduct.specific_prices.reduction * 100}%<b>{l s='off' mod='productscategory'}</b></span>
						{/if}
					{/if}
					</div>
					
					{hook h="displayProductDeliveryTime" product=$categoryProduct}
					{hook h="displayProductPriceBlock" product=$categoryProduct type="weight"}
				</div>
				<div class="right-block">
					{if (!$PS_CATALOG_MODE AND ((isset($categoryProduct.show_price) && $categoryProduct.show_price) || (isset($categoryProduct.available_for_order) && $categoryProduct.available_for_order)))}
							<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
								{if isset($categoryProduct.show_price) && $categoryProduct.show_price && !isset($restricted_country_mode)}
									<span itemprop="price" class="price product-price">
										{if !$priceDisplay}{convertPrice price=$categoryProduct.price}{else}{convertPrice price=$categoryProduct.price_tax_exc}{/if}
									</span>
									<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
									{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices && isset($categoryProduct.specific_prices.reduction) && $categoryProduct.specific_prices.reduction > 0}
										{hook h="displayProductPriceBlock" product=$categoryProduct type="old_price"}
										<span class="old-price product-price">
											{displayWtPrice p=$categoryProduct.price_without_reduction}
										</span>
										{*{if $categoryProduct.specific_prices.reduction_type == 'percentage'}
											<span class="tag price-percent-reduction">{$categoryProduct.specific_prices.reduction * 100}%<b>{l s='off' mod='productscategory'}</b></span>
										{/if}*}
									{/if}
									{hook h="displayProductPriceBlock" product=$categoryProduct type="price"}
									{hook h="displayProductPriceBlock" product=$categoryProduct type="unit_price"}
									 {hook h="displayProductPriceBlock" product=$product type='after_price'}
								{/if}
							</div>
						{/if}
						{hook h='displayProductListReviews' product=$categoryProduct}
					<h5 itemprop="name">
						{if isset($categoryProduct.pack_quantity) && $categoryProduct.pack_quantity}{$categoryProduct.pack_quantity|intval|cat:' x '}{/if}
						<a class="product-name" href="{$categoryProduct.link|escape:'html':'UTF-8'}" title="{$categoryProduct.name|escape:'html':'UTF-8'}" itemprop="url" >
							{$categoryProduct.name|truncate:45:'...'|escape:'html':'UTF-8'}
						</a>
					</h5>
					
					<p class="product-desc" itemprop="description">
						{$categoryProduct.description_short|strip_tags:'UTF-8'|truncate:60:'...'}
					</p>
				</div>
			</div><!-- .product-container> -->
		</li>
		{/foreach}
		</ul>
	</div>
</section>
{/if}