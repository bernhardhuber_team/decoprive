<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productscategory}interior>productscategory_8a4f5a66d0fcc9d13614516db6e3d47a'] = 'Товары в той же категории';
$_MODULE['<{productscategory}interior>productscategory_1d269d7f013c3d9d891a146f4379eb02'] = 'Добавить блок на страницу товара, отображающий товары из той же категории.';
$_MODULE['<{productscategory}interior>productscategory_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Настройки обновлены.';
$_MODULE['<{productscategory}interior>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{productscategory}interior>productscategory_e06ba84b50810a88438ae0537405f65a'] = 'Отображать цены товаров';
$_MODULE['<{productscategory}interior>productscategory_1d986024f548d57b1d743ec7ea9b09d9'] = 'Показывать цены товаров, появляющихся в этом блоке.';
$_MODULE['<{productscategory}interior>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включено';
$_MODULE['<{productscategory}interior>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Отключено';
$_MODULE['<{productscategory}interior>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{productscategory}interior>productscategory_1f910bcf84a92cb7c71fa3d926c8a525'] = 'Другие товары в этой категории';
$_MODULE['<{productscategory}interior>productscategory_dd1f775e443ff3b9a89270713580a51b'] = 'Назад';
$_MODULE['<{productscategory}interior>productscategory_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Далее';
$_MODULE['<{productscategory}interior>productscategory_7b15d58b169bc5a677bcdcae7ce4399b'] = 'Другие продукты';
$_MODULE['<{productscategory}interior>productscategory_03c2e7e41ffc181a4e84080b4710e81e'] = 'Новые';
$_MODULE['<{productscategory}interior>productscategory_bb63f16d5ebfcfa8a651642a7bb2ea5c'] = 'Sale!';
$_MODULE['<{productscategory}interior>productscategory_69d08bd5f8cf4e228930935c3f13e42f'] = 'В Наличии';
$_MODULE['<{productscategory}interior>productscategory_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'На складе';
$_MODULE['<{productscategory}interior>productscategory_74eebd447817f31f7421b12077c97869'] = 'Нет ';
$_MODULE['<{productscategory}interior>productscategory_908880209a64ea539ae8dc5fdb7e0a91'] = 'на складе';
$_MODULE['<{productscategory}interior>productscategory_3262d48df5d75e3452f0f16b313b7808'] = 'off';
