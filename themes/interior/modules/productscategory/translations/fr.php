<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productscategory}interior>productscategory_8a4f5a66d0fcc9d13614516db6e3d47a'] = 'Produits dans la même catégorie';
$_MODULE['<{productscategory}interior>productscategory_1d269d7f013c3d9d891a146f4379eb02'] = 'Ajoute un bloc sur la fiche produit pour afficher des produits de la même catégorie.';
$_MODULE['<{productscategory}interior>productscategory_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Paramètres mis à jour avec succès';
$_MODULE['<{productscategory}interior>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{productscategory}interior>productscategory_e06ba84b50810a88438ae0537405f65a'] = 'Afficher les prix des produits';
$_MODULE['<{productscategory}interior>productscategory_1d986024f548d57b1d743ec7ea9b09d9'] = 'Afficher les prix des produits affichés dans le bloc.';
$_MODULE['<{productscategory}interior>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{productscategory}interior>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{productscategory}interior>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{productscategory}interior>productscategory_f55e0a28b86c2ab66ac632ab9ddf1833'] = '%s autre produit dans la même catégorie :';
$_MODULE['<{productscategory}interior>productscategory_bebb44f38b03407098d48198c1d0aaa5'] = '%s autres produits dans la même catégorie :';
$_MODULE['<{productscategory}interior>productscategory_dd1f775e443ff3b9a89270713580a51b'] = 'Précédent';
$_MODULE['<{productscategory}interior>productscategory_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{productscategory}interior>productscategory_7b15d58b169bc5a677bcdcae7ce4399b'] = 'autres produits';
$_MODULE['<{productscategory}interior>productscategory_03c2e7e41ffc181a4e84080b4710e81e'] = 'nouveau';
$_MODULE['<{productscategory}interior>productscategory_bb63f16d5ebfcfa8a651642a7bb2ea5c'] = 'Vente!';
$_MODULE['<{productscategory}interior>productscategory_69d08bd5f8cf4e228930935c3f13e42f'] = 'rupture de stock';
$_MODULE['<{productscategory}interior>productscategory_b55197a49e8c4cd8c314bc2aa39d6feb'] = 'Rupture de stock';
