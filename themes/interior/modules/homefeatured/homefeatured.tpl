{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<h2 class="title_main_section carousel_title">
		<span>{l s='Popular' mod='homefeatured'}</span>
</h2>
{if isset($products) && $products}
	{include file="$tpl_dir./product-list.tpl" class='homefeatured masonry' id='homefeatured'}
	<div class="link_more">
        	<a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='All featured' mod='homefeatured'}"  class="link_style"><span>{l s='See all products' mod='homefeatured'}</span>
        	</a>
</div>
{else}
<ul id="homefeatured" class="homefeatured">
	<li class="alert alert-info">{l s='No featured products at this time.' mod='homefeatured'}</li>
</ul>
{/if}