<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{custombanners}interior>custombanners_d15bdc03bbd610d5bd515c6ee18842ef'] = 'Des bannières personnalisées';
$_MODULE['<{custombanners}interior>custombanners_d2d72c90bb88220a1e17c2eae0b8c4db'] = 'Table de base de données n\'a pas été installé correctement';
$_MODULE['<{custombanners}interior>custombanners_248336101b461380a4b2391a7625493d'] = 'Enregistré';
$_MODULE['<{custombanners}interior>custombanners_786862a902f7e77bdfa1203980ee65c5'] = 'Fichier n\'est pas téléchargé';
$_MODULE['<{custombanners}interior>custombanners_1a05c9ea12f7e3428686971509901b52'] = 'S\'il vous plaît télécharger valide d\'un fichier zip';
$_MODULE['<{custombanners}interior>custombanners_6f7c8c5e696c57ba411c2680c90e88dd'] = 'Une erreur s\'est produite lors de la décompression de l\'archive';
$_MODULE['<{custombanners}interior>custombanners_d16cad9eb2f8568e7c4eda91e99506d3'] = 'Ce n\'est pas un fichier de sauvegarde valide';
$_MODULE['<{custombanners}interior>banner-form_f09e85ba0c7da5236595a35d1d6ddab8'] = 'Dans le carrousel';
$_MODULE['<{custombanners}interior>banner-form_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Active';
$_MODULE['<{custombanners}interior>banner-form_7dce122004969d56ae2e0245cb754d35'] = 'Modifier';
$_MODULE['<{custombanners}interior>banner-form_739fe21361b62c493605f8a740f361d7'] = 'Faites Défiler Vers Le Haut';
$_MODULE['<{custombanners}interior>banner-form_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{custombanners}interior>banner-form_8e2f2e0e26079bdffee4a7c1e58217d5'] = 'Télécharger de nouvelles';
$_MODULE['<{custombanners}interior>banner-form_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{custombanners}interior>configure_de62775a71fc2bf7a13d7530ae24a7ed'] = 'Paramètres généraux';
$_MODULE['<{custombanners}interior>configure_01fb8f9149136834af11186e89c8d7e1'] = 'Exportation de toutes les bannières';
$_MODULE['<{custombanners}interior>configure_a242b82802896bb27937135d0fa0f42e'] = 'Importer des bannières';
$_MODULE['<{custombanners}interior>configure_3d6954dd72e53b9015d2a6e6546058f8'] = 'ALLER';
$_MODULE['<{custombanners}interior>configure_a54f98b0e23e6925c855760cdabd7168'] = 'Bannières';
$_MODULE['<{custombanners}interior>configure_5a5ee220c12193cbe869af4fc6faaa09'] = 'Sélectionnez crochet';
$_MODULE['<{custombanners}interior>configure_43855b46a2cae8034a0fd6461cb1a023'] = 'Ajouter la nouvelle bannière';
$_MODULE['<{custombanners}interior>configure_3e1cd8c322f52423c04f6acca24e6afc'] = 'Pour afficher ce crochet, insérez le code suivant à tout tpl';
