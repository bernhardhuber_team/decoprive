{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA 
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block permanent links module HEADER -->
<div class="module-header-links">
	<a id="drop_links" href="#" title="{l s='List of links user info' mod='blockpermanentlinks'}">
		<i class="icon-link"></i>
	</a>
	<ul id="header_links" class="dropdown-links">
		
		<li id="header_link_about_us"><a href="{$base_dir}" title="{l s='Accueil' mod='blockpermanentlinks'}">{l s='Accueil' mod='blockpermanentlinks'}</a></li>		
		<li id="header_link_about_us"><a href="{$link->getCMSLink(32)}" title="{l s='Références' mod='blockpermanentlinks'}">{l s='Références' mod='blockpermanentlinks'}</a></li>		
		<li id="header_link_about_us"><a href="{$link->getCMSLink(2)}" title="{l s='Show Room' mod='blockpermanentlinks'}">{l s='Show Room' mod='blockpermanentlinks'}</a></li>		
		<li id="header_link_about_us"><a href="{$link->getCMSLink(7)}" title="{l s='Espace Pros' mod='blockpermanentlinks'}">{l s='Espace Pros' mod='blockpermanentlinks'}</a></li>		
		
		{*<li id="header_link_about_us"><a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='About us' mod='blockpermanentlinks'}">{l s='About us' mod='blockpermanentlinks'}</a></li>*}
		
		<li id="header_link_catalog"><a href="{$link->getCMSLink(56)}" title="{l s='Catalogues' mod='blockpermanentlinks'}">{l s='Catalogues' mod='blockpermanentlinks'}</a></li>
		<li id="header_link_news"><a href="{$link->getCMSLink(4)}" title="{l s='Vous souhaitez louer notre mobilier ?' mod='blockpermanentlinks'}">{l s='Vous souhaitez louer notre mobilier ?' mod='blockpermanentlinks'}</a></li>		
		
		<li id="header_link_sitemap"><a href="{$base_dir}blog_deco" title="{l s='Blog' mod='blockpermanentlinks'}">{l s='Blog' mod='blockpermanentlinks'}</a></li>
		
		<li id="header_link_sitemap"><a href="{$link->getPageLink('sitemap')|escape:'html'}" title="{l s='sitemap' mod='blockpermanentlinks'}">{l s='sitemap' mod='blockpermanentlinks'}</a></li>
		<li id="header_link_contact"><a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='contact' mod='blockpermanentlinks'}">{l s='contact' mod='blockpermanentlinks'}</a></li>
		{*<li id="header_link_bookmark">
			<script type="text/javascript">writeBookmarkLink('{$come_from}', '{$meta_title|addslashes|addslashes}', '{l s='bookmark' mod='blockpermanentlinks' js=1}');</script>
		</li>*}
	</ul>
</div>
<!-- /Block permanent links module HEADER -->
