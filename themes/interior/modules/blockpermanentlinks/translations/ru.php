<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_5813ce0ec7196c492c97596718f71969'] = 'Карта сайта';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_bbaff12800505b22a853e8b7f4eb6a22'] = 'Контакты';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_714814d37531916c293a8a4007e8418c'] = 'Добавить страницу в закладки.';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_2f8a6bf31f3bd67bd2d9720c58b19c9a'] = 'Контакты';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_e1da49db34b0bdfdddaba2ad6552f848'] = 'Карта сайта';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_fad9383ed4698856ed467fd49ecf4820'] = 'В закладки';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_39355c36cfd8f1d048a1f84803963534'] = 'Блок постоянных ссылок';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_6c5b993889148d10481569e55f8f7c6d'] = 'Добавляет блок, отображающий постоянные ссылки, например - контакты, карта сайта и т.п.';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c46000dc35dcefd6cc33237fa52acc48'] = 'Карта магазина';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_5813ce0ec7196c492c97596718f71969'] = 'Карта сайта';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c661cf76442d8d2cb318d560285a2a57'] = 'Обратная связь';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_bbaff12800505b22a853e8b7f4eb6a22'] = 'Контакты';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_2d29fbe96aaeb9737b355192f4683690'] = 'Добавить страницу в закладки';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_09a763df3edf590f70369bf8f7b65222'] = 'Список ссылок user info';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_cf23ee279844016288ea1c076638f3be'] = 'О нас ';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_c32516babc5b6c47eb8ce1bfc223253c'] = 'Каталог';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_5866a0604fba58ef14b60f85f5d3a938'] = 'Новости ';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_cf23ee279844016288ea1c076638f3be'] = 'О нас';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c32516babc5b6c47eb8ce1bfc223253c'] = 'Каталог';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_eba53a2975a1944b1020754ffecaf389'] = 'Новости и пресс';
