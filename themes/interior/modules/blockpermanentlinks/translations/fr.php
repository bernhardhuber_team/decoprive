<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_5813ce0ec7196c492c97596718f71969'] = 'sitemap';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contact';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-footer_714814d37531916c293a8a4007e8418c'] = 'Mettre cette page en favori';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_2f8a6bf31f3bd67bd2d9720c58b19c9a'] = 'Contact';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_e1da49db34b0bdfdddaba2ad6552f848'] = 'Plan du site';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_fad9383ed4698856ed467fd49ecf4820'] = 'Favoris';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_39355c36cfd8f1d048a1f84803963534'] = 'Bloc liens permanents';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_6c5b993889148d10481569e55f8f7c6d'] = 'Ajoute un bloc qui affiche des liens permanents (contact, plan du site, etc.).';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c46000dc35dcefd6cc33237fa52acc48'] = 'plan de la boutique';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_5813ce0ec7196c492c97596718f71969'] = 'sitemap';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c661cf76442d8d2cb318d560285a2a57'] = 'Formulaire de contact';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contact';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_2d29fbe96aaeb9737b355192f4683690'] = 'Mettre cette page en favori';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_09a763df3edf590f70369bf8f7b65222'] = 'Liste de liens userinfo';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_cf23ee279844016288ea1c076638f3be'] = 'à propos de nous';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_c32516babc5b6c47eb8ce1bfc223253c'] = 'Catalogue';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks-header_5866a0604fba58ef14b60f85f5d3a938'] = 'News & Press';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_cf23ee279844016288ea1c076638f3be'] = 'à propos de nous';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_c32516babc5b6c47eb8ce1bfc223253c'] = 'catalogue';
$_MODULE['<{blockpermanentlinks}interior>blockpermanentlinks_eba53a2975a1944b1020754ffecaf389'] = 'Nouvelles & Presse';
