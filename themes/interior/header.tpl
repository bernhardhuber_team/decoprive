{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->
<html lang="{$lang_iso}">
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" /> 
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
{if isset($css_files)}
	{foreach from=$css_files key=css_uri item=media}
		<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
	{/foreach}
{/if}
{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
		{$HOOK_HEADER}
		<link href='http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:400italic,800italic,700,300,600,800,400&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
		
		
		{literal}
	<style>
	.cookiespopup {
		background-color: rgba(0, 0, 0, 0.7);
		bottom: 0;
		box-sizing: border-box;
		color: #fff;
		font-family: arial,sans-serif;
		font-size: 12px;
		left: 0;
		opacity: 0.7;
		padding: 10px 40px;
		position: fixed;
		text-align: center;
		text-transform: uppercase;
		width: 100%;
		z-index: 100;
	}
	.cookiespopup .cookiespopupclosebtn {
		background: rgba(0, 0, 0, 0) url("http://www.deco-prive.com/img/close.png") no-repeat scroll 0 0;
		height: 48px;
		margin-top: -24px;
		padding: 10px;
		position: absolute;
		right: 295px;
		text-indent: -9999px;
		top: 50%;
		width: 48px;
		z-index: 200;
	}
	#footer_cookie_bar_block .ilightbox_popin{color: #FFF !important;}
	#footer_cookie_bar_block .ilightbox_popin:hover{color: #952e32 !important;}
	</style>
	{/literal}
		
		
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	
	
	{literal}
	<script>
		function checkCookie(){
			var x = readCookie('cookiebar');
			if(x!="yes"){
				document.getElementById("footer_cookie_bar_block").style.display = '';
			}
		}
		function readCookie(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1,c.length);
				if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			}
			return null;
		}
	</script>
	{/literal}
	
	
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span></p>
			</div>
		{/if}
		<div id="page">
			{*
			<div class="header-container">
				
			</div>
			*}
			{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column nopadding col-xs-12 col-md-{$left_column_size|intval}">
							<div class="container_left">
								<div class="navbar-header">
								    <a class="navbar-toggle"> 
								     </a>
								</div>
								<div id="slidemenu">
								<div id="header_logo">
									<a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"/>
									</a>
								</div>
								
								
								<div class="shop_phn_left"><h2><span style="color: #d0121a;"><em>{l s='Ouvert non stop du lundi au vendredi'}<br></em></span></h2></div>
								<div class="shop_phn_left">
									<h2><span style="color: #000000;"><em>
									{l s='Tél :'} 
									{if $get_custom_shop_id==1} {l s='01 48 43 00 00'}{/if}
									{if $get_custom_shop_id==2}{l s='01 48 43 96 14'}{/if}									
									 </em></span></h2>
								</div>
								
								
								
									{$HOOK_LEFT_COLUMN}
								</div>
							</div>
							<div class="mark"></div>
						</div>
						{/if}
				<div id="columns" class="container-fluid">
					<div id="header" class="nav top">
						{*<nav class="row">{hook h="displayNav"}</nav>*}
						
						<nav class="row">
					
							{$get_top_menu_obj->content}
						
						
						{hook h="displayNav"}
						</nav>
						
					</div>
						{if $page_name == 'index'}
							{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
								<div id="displayTopHeader" class="nopadding dispayTopHeader clearfix col-xs-12">
									{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
							</div>
						{/if}
						<div id="center_column" class="center_column clearfix col-xs-12 col-md-{$cols|intval}">
						{if $page_name !='index' && $page_name !='pagenotfound'}
								{include file="$tpl_dir./breadcrumb.tpl"}
						{/if}
						{if $page_name == index}
							<div class="nopadding col-xs-12">
									{hook h="DisplayTopColumn"}
							</div>
						{/if}
	{/if}