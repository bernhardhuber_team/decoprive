/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$(document).ready(function(){
	$('ul.tree.dhtml').hide();

	//to do not execute this script as much as it's called...
	if(!$('ul.tree.dhtml').hasClass('dynamized'))
	{
		//add growers to each ul.tree elements
		$('ul.tree.dhtml ul').prev().before("<span class='grower OPEN'> </span>");
		
		//dynamically add the '.last' class on each last item of a branch
		$('ul.tree.dhtml ul li:last-child, ul.tree.dhtml li:last-child').addClass('last');
		
		//collapse every expanded branch
		$('ul.tree.dhtml span.grower.OPEN').addClass('CLOSE').removeClass('OPEN').parent().find('ul:first').hide();
		$('ul.tree.dhtml').show();
		
		//open the tree for the selected branch
			$('ul.tree.dhtml .selected').parents().each( function() {
				if ($(this).is('ul'))
					toggleBranch($(this).prev().prev(), true);
			});
			toggleBranch( $('ul.tree.dhtml .selected').prev(), true);
		
		//add a fonction on clicks on growers
		$('ul.tree.dhtml span.grower').click(function(){
				toggleBranch($(this));
				/*Cusmom js*/
				setTimeout(function(){
					$(window).resize();
				},500); 
				/*end Cusmom js*/
		});
		//mark this 'ul.tree' elements as already 'dynamized'
		$('ul.tree.dhtml').addClass('dynamized');

		$('ul.tree.dhtml').removeClass('dhtml');
	}
	/*Cusmom js*/
	wrapperDropCategory();
	setHeightDrop();
	$(window).resize(setHeightDrop);
	/*end Cusmom js*/
});
/*Cusmom js*/
function wrapperDropCategory(){
	$('#categories_block_left ul li ul').wrap('<div class="wrap-drop-category"></div>');
}

function setHeightDrop(){
	var heightWindow = $(window).height();
	var heightVisible = $(window).height();
	var elemUl = $('.block_content_category').children('ul').children('li').children('.wrap-drop-category').children('ul');
	if(scrollCompensate() + $(window).width()>1024){
		$('.block_content_category').children('ul').children('li').children('.wrap-drop-category').height(heightWindow);
		elemUl.each(function(){
			var heightMenuItem = $(this).parent().prev().outerHeight();
			var herghtElemUl = $(this).outerHeight();
			if (herghtElemUl >= heightVisible - 150){
				$(this).css({top:'0',marginTop:'0'});
			}else{
				var offsetTopUl = $('.navbar-header').offset().top - $(this).parent().parent().offset().top - $(this).parent().parent().outerHeight();
				$(this).css({'top':Math.abs(offsetTopUl)-heightMenuItem,'margin-top':-herghtElemUl/2 + heightMenuItem/2});
			}
		});
	}else {
		$('.block_content_category').children('ul').children('li').children('.wrap-drop-category').css('height','auto');
		elemUl.css('');
	}
}
/*end Cusmom js*/
//animate the opening of the branch (span.grower jQueryElement)
function openBranch(jQueryElement, noAnimation)
{
		jQueryElement.addClass('OPEN').removeClass('CLOSE');
		jQueryElement.parent('li').removeClass('CLOSE').addClass('OPEN');
		if(noAnimation)
			jQueryElement.parent().find('ul:first').show();
		else
			jQueryElement.parent().find('ul:first').slideDown();
}
//animate the closing of the branch (span.grower jQueryElement)
function closeBranch(jQueryElement, noAnimation)
{
	jQueryElement.addClass('CLOSE').removeClass('OPEN');
	jQueryElement.parent('li').addClass('CLOSE').removeClass('OPEN');
	if(noAnimation)
		jQueryElement.parent().find('ul:first').hide();
	else
		jQueryElement.parent().find('ul:first').slideUp();
}

//animate the closing or opening of the branch (ul jQueryElement)
function toggleBranch(jQueryElement, noAnimation)
{
	if(jQueryElement.hasClass('OPEN'))
		closeBranch(jQueryElement, noAnimation);
	else
		openBranch(jQueryElement, noAnimation);
}