$(document).ready(function () {


    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#left_column .container_left').append($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#columns';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '320px';
    var menuneg = '-320px';
    var slideneg = '-320px';

  $("#left_column .navbar-header").click(function(){
        setTimeout(function(){
            $(window).resize();
        },200);
        
    });
    $("#left_column").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().css({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().css({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().css({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().css({
            left: selected ? '0px' : '100%'
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#columns, #left_column, body, .navbar-header').toggleClass('slide-active');


    });


    var selected = '#slidemenu, #columns, body, #left_column, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 1024 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }


    });




});