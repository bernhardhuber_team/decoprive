$(document).ready(function(){
	addDropDown();
	dropDownHeaderLinks();
	heightLeftMenu();
	widthSlideMenu();
	responsiveKwicks();
	onFocusSearch();
	setTimeout(function(){
		positionBlock();
		$(window).resize();
	},700);
		scrollCarousel();
	$(window).resize(function(){
		addDropDown();
		responsiveKwicks();
		heightLeftMenu();
		widthSlideMenu();
		positionBlock();
		onFocusSearch();
	});
	$('#layered_block_left').prependTo('.content_sortPagiBar.top_sogi');

});
function scrollCarousel(){
	$('.scroll_container,.container_left').each(
		function()
		{
			$(this).jScrollPane(
				{
					showArrows: true,
					horizontalArrowPositions: 'before',
					speed:265
				}
			);
			var api = $(this).data('jsp');
			var throttleTimeout;
			$(window).bind(
				'resize',
				function()
				{
					if (!throttleTimeout) {
						throttleTimeout = setTimeout(
							function()
							{
								api.reinitialise();
								throttleTimeout = null;
							},
							50
						);
					}
				}
			);
		}
	)
}
function dropDownHeaderLinks(){
	$("#drop_links").click(function(){
		if($(".module-header-links").hasClass("opened")) {
			$(this).next(".dropdown-links").slideUp();
			$(this).parent().removeClass("opened");
			return false;
			}
		else{
		$(this).parent().addClass("opened");
		$(this).next(".dropdown-links").slideDown();
		return false;
		}
	});
	$(document).on('click',function(e){
		 if ($(e.target).closest('.module-header-links,.header_user_nav').length) return; 
		$('.dropdown-links').slideUp();
		$(".dropdown-links").removeClass("opened");
		e.stopPropagation();
	});

	$("#drop_links_user").click(function(){
		if($(".header_user_nav").hasClass("opened")) {
			$(this).next(".dropdown-links").slideUp();
			$(this).parent().removeClass("opened");
			return false;
			}
		else{
		$(this).parent().addClass("opened");
		$(this).next(".dropdown-links").slideDown();
		return false;
		}
	});
	$(document).on('click',function(e){
		 if ($(e.target).closest('.module-header-links,.header_user_nav').length) return; 
		$('.dropdown-links').slideUp();
		$(".dropdown-links").removeClass("opened");
		e.stopPropagation();
	});
}
function addDropDown(){
		if(scrollCompensate() + $(window).width()<1366){
				$('#header_links').css('display','none').addClass('dropdown-links');
		}else{
				$('#header_links').css('display','block').removeClass('dropdown-links');
		}
		if(scrollCompensate() + $(window).width()<768){
				$('.header_user_nav #header_nav').css('display','none').addClass('dropdown-links');
		}else{
				$('.header_user_nav #header_nav').css('display','block').removeClass('dropdown-links');
		}

}
function maxHeightKwicks(){
	var itemsKwicks = [];
		$('.kwicks .banner-item').each(function(){
			var heightItem = $(this).outerHeight();
			itemsKwicks.push(heightItem);
		});
	var maxHeightKwicks = Math.max.apply(Math,itemsKwicks);
	return maxHeightKwicks;
}
function responsiveKwicks(){
	var itemKwick = $('.kwicks');
	if(scrollCompensate() + $(window).width()>767){
		if(itemKwick.hasClass('owl-carousel')){
			itemKwick.data('owlCarousel').destroy();
		}
		var heightKwicksContaeiner = maxHeightKwicks();
		itemKwick.outerHeight(heightKwicksContaeiner);
		itemKwick.kwicks({
			maxSize: '30%',
			behavior: 'slideshow'
		});
	}else{
		if(itemKwick.hasClass('kwicks-processed')){
			itemKwick.kwicks('destroy');
		}
		itemKwick.owlCarousel({
			navigation:true,
			pagination:false,
			navigationText:[],
			items : 2,
			itemsDesktop : [1199,2],
			itemsDesktopSmall : [980,2],
			itemsTablet: [768,2],
			itemsMobile : [479,1]
		});
	}
}

function heightLeftMenu(){
	var heightWindow = $(window).outerHeight();
	$('.container_left').css('height',heightWindow + 'px');
}
function widthSlideMenu(){
	if(scrollCompensate() + $(window).width() > 1024){
		$('#slidemenu').css('width',$('#left_column').outerWidth()-1);
	}
	else{
		$('#slidemenu').css('width','');
	}
}
function positionBlock(){
	var heightWindow = $(window).outerHeight();
	var heightJspPane = $('.container_left .jspPane').outerHeight();
	var blockContact = $('.custombanners.displayLeftColumn');
	var blockHeight = blockContact.outerHeight();
	if (blockContact.length != 0){
		var blockOffsetTop = blockContact.offset().top;
		var markOffsetTop = $('#left_column .mark').offset().top;
	}
	var topMove = markOffsetTop - blockOffsetTop - blockHeight;
	/*console.log(topMove);
	console.log(blockOffsetTop);
	console.log(markOffsetTop);*/
	if (heightJspPane + blockHeight < heightWindow){
		if(topMove > 0)
			blockContact. css('margin-top',topMove + 'px');
			blockContact.css('position','absolute');
		}
	else{
		blockContact.css('margin-top','');
		blockContact.css('position','relative');
	}
}
/*Фиксирование меню при прокрутке*/

	 var isTouch = function() {
					return $(window).width() < 0 ? true : false;
			};
			var determineScreenClass = function() {
					$("html").toggleClass("large-screen", !isTouch());
			};
			//  ========== 
			//  = Scroll inspector = 
			//  ========== 
			var stickyNavbar = function() {
					if (isTouch()) {
							$(window).off("scroll.onlyDesktop");
					} else {
							var $headerHeight = $("#header").height();
							$(window).on("scroll.onlyDesktop", function() {
									var scrollX = $(window).scrollTop();
									var mTop = $headerHeight/2;
									if (scrollX > mTop && ($(window).width() + scrollCompensate()) < 581) {
											$("#header").addClass('noShadow');
									} else {
											$("#header").removeClass('noShadow');

									}
							});
					}
			};
			//  ========== 
			//  = Functions which has to be reinitiated when the window size is changed = 
			//  ==========
			var triggeredOnResize = function() {
					// sticky navbar
					stickyNavbar();
			};
			var fromLastResize;
			// counter in miliseconds
			$(window).resize(function() {
					determineScreenClass();
					clearTimeout(fromLastResize);
					fromLastResize = setTimeout(function() {
							triggeredOnResize();
					}, 250);
			});

			//  ========== 
			//  = Last but not the least - trigger the page scroll and resize = 
			//  ========== 
			$(window).trigger("scroll").trigger("resize");

function onFocusSearch(){
	var isSearch = $('.search_query');
	if (isSearch !== null){
		if(scrollCompensate() + $(window).width()<768){
			$('.search_query').focus(function(){
				$(this).parent().parent('.search-box').addClass('search-active');
			});
			$('.search_query').focusout(function(){
				$(this).parent().parent('.search-box').removeClass('search-active');
			});
		}else{
			$('.search_query').off();
			$('.search-box').removeClass('search-active');
		}
	}
}
