<?php

class ProductController extends ProductControllercore
{

 public function initContent()
	{
		parent::initContent();
		$productCategories = Product::getProductCategoriesFull($this->product->id, Context::getContext()->language->id);
		$demandededevis = false;
		foreach($productCategories as $category)
		{
		  if($this->context->shop->id ==2)
		  $demandededevis = true;
		}
		$this->context->smarty->assign(array('demandededevis' => $demandededevis));
		
	}
}
