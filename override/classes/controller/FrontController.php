<?php

class FrontController extends FrontControllerCore
{  
    public function initContent()
    {
        
		$get_top_menu_obj=new CMS(64, $this->context->cookie->id_lang);
		
		//echo $get_top_menu_obj->content;
		
		$this->context->smarty->assign(array(
				'get_top_menu_obj' => $get_top_menu_obj,
				'get_custom_shop_id' => $this->context->shop->id,
			));
			
		parent::initContent();
	}
	
	
}
